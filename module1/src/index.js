import 'config/ReactotronConfig';
import 'config/DevToolsConfig';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  Button,
  View,
} from 'react-native';

import Todo from './components/Todo';

export default class App extends Component {
  state = {
    usuario: 'Arthur',
    todos: [
      { id: 0, text: 'Fazer café' },
      { id: 1, text: 'Estudar React Native' },
    ],
  }

  addTodo = () => {
    this.setState({
      todos: [...this.state.todos, { id: Math.random, text: 'Estudar JS !' }],
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>{ this.state.usuario }</Text>
        {this.state.todos.map(todo => (<Todo key={todo.id} title={todo.text} style={styles.box} />))}
        <Button title="Adicionar todo" onPress={this.addTodo} />
        <View style={styles.box} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee0ee',
  },
  box: {
    width: 80,
    height: 80,
    margin: 10,
    backgroundColor: '#bb000e',
  },
});
