import { StyleSheet } from 'react-native';
import { general, metrics, colors } from 'styles';

const styles = StyleSheet.create({
  container: {
    ...general.box,
    marginHorizontal: metrics.basePadding,
    marginTop: metrics.baseMargin,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 14,
  },
  info: {
    flexDirection: 'row',
    marginRight: metrics.baseMargin,
    alignItems: 'center',
  },
  infoText: {
    marginLeft: metrics.baseMargin / 2,
    fontSize: 12,
    color: colors.dark,
  },
  infoContainer: {
    flexDirection: 'row',
    marginTop: metrics.baseMargin,
  },
  infoIcon: {
    color: colors.dark,
  },
});

export default styles;
